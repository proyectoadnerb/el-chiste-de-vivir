extends Node2D

var arbol_r=preload('res://arbol.tscn')

func _ready():

	for x in range(1,32):
		for y in range(0,32):
#			$suelo.set_cell(x,y,randi()%5) #para cuando tenga mas de 1 tipo de suelo 
			match juego.map_bosque[y][x]:
				0:
					var arbol=arbol_r.instance()
					arbol.obj=0
					add_child(arbol)
					
					arbol.global_position=Vector2(x*32+16,y*32+16)
				1:
					var arbol=arbol_r.instance()
					arbol.obj=1
					add_child(arbol)
					
					arbol.global_position=Vector2(x*32+16,y*32+16)

func _on_puente_area_entered(area):
	if area.name=='jugador':
		juego.jug_pos=Vector2(48,368)
		get_tree().change_scene('res://ciudad.tscn')
