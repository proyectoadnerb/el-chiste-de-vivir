extends KinematicBody2D
#180 madera
#66 piedra
var velocidad=Vector2()
var dir=Vector2()
var jug_pos=Vector2()
var obj
var vel=1.0
func _ready():
	set_physics_process(false)


func _physics_process(delta):
	
	velocidad=move_and_slide(dir*delta*vel)
	vel+=25
	seguir_jugador()
	
	if dir.length()<5:
		queue_free()

func seguir_jugador():
	jug_pos=get_tree().get_nodes_in_group('jugador')[0].global_position
	dir=jug_pos-global_position
	dir.normalized()

func _on_detector_area_entered(area):
	if area.name=='jugador':
		seguir_jugador()
		set_physics_process(true)


func _on_item_area_entered(area):
	if area.name=='jugador':
		queue_free()
