extends Node
var map_bosque=matriz(32,32)
var jug_pos=Vector2()
func _ready():
	jug_pos=Vector2(16,16)
	gen_map()

func gen_map():
	randomize()
	for x in range(0,32):
		for y in range(0,32):
			map_bosque[y][x]=randi()%10

func matriz(width, height):
	var a = []
	var value=0
	for y in range(0,height):
		a.append([])
		a[y].resize(width)
		
		for x in range(0,width):
			a[y][x] = value
	return a