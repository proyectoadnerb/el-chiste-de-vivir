extends StaticBody2D
var sprite_arbol='res://recursos/graficos/arbol.png'
var sprite_roca='res://recursos/graficos/roca.png'
var item=load('res://items/item.tscn')
var mouse_adentro=false
var jug_adentro=false
var obj #codigo de objeto
var resistencia #que tan duro es va depender del codigo de objeto


func _ready():
	
	if obj==0: #arbol
		$spr.texture=load(sprite_arbol)
		resistencia=10
	elif obj==1: #roca
		$spr.texture=load(sprite_roca)
		resistencia=15
	$barra.max_value=resistencia
	$barra.value=resistencia

func _input(event):
	if event is InputEventMouseButton and mouse_adentro:
		if event.is_pressed() and jug_adentro:
			$anim.play("golpe")
			resistencia-=1
			$barra.value=resistencia
			$particulas.global_position=get_global_mouse_position()
			$particulas.emitting=true
			
			if resistencia==0:
				var item_ins=item.instance()
				item_ins.global_position=global_position
				item_ins.obj=obj
				get_parent().add_child(item_ins)
				queue_free()

func _on_impacto_mouse_entered():
	mouse_adentro=true


func _on_impacto_mouse_exited():
	mouse_adentro=false


func _on_detector_jug_area_entered(area):
	if area.name=='jugador':
		jug_adentro=true


func _on_detector_jug_area_exited(area):
	if area.name=='jugador':
		jug_adentro=false
