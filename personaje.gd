extends Node2D
const arriba=Vector2(0,-32)
const abajo=Vector2(0,32)
const izquierda=Vector2(-32,0)
const derecha=Vector2(32,0)

const quieto=0
const moviendo=1

var puede_mover=true
var direccion=Vector2()
var estado=quieto

var anim='abajo'

func _ready():
	global_position=juego.jug_pos

func _physics_process(delta):
	if Input.is_action_just_pressed("ui_accept"):
		get_tree().reload_current_scene()
	match estado:
		quieto: 
			if Input.is_action_pressed("abajo") and !$ray/abajo.is_colliding():
				direccion=abajo
				estado=moviendo
				anim='abajo'
			elif Input.is_action_pressed("arriba") and !$ray/arriba.is_colliding():
				direccion=arriba
				estado=moviendo
				anim='arriba'
			elif Input.is_action_pressed("izquierda") and !$ray/izquierda.is_colliding():
				direccion=izquierda
				estado=moviendo
			elif Input.is_action_pressed("derecha") and !$ray/derecha.is_colliding():
				direccion=derecha
				estado=moviendo
				
		moviendo:
			$anim.play('caminar_'+anim)
			if puede_mover:
				mover(direccion)
			
			if Input.is_action_just_released("abajo") or ($ray/abajo.is_colliding() and direccion==abajo):
				estado=quieto
			elif Input.is_action_just_released("arriba") or ($ray/arriba.is_colliding() and direccion==arriba):
				estado=quieto
			elif Input.is_action_just_released("izquierda") or ($ray/izquierda.is_colliding() and direccion==izquierda):
				estado=quieto
			elif Input.is_action_just_released("derecha") or ($ray/derecha.is_colliding() and direccion==derecha):
				estado=quieto

func mover(dir):
	
	puede_mover=false
	estado=moviendo
	$Tween.interpolate_property(self, "global_position",
	global_position, global_position+dir, .4,
	Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	$Tween.start()
	
	yield($Tween,"tween_completed")
	puede_mover=true
	$anim.play('quieto_'+anim)
	
	
